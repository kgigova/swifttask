# Swift MT799 Web API
Welcome to the Swift MT799 Web API project! This Web API is designed to accept, parse, and store Swift MT799 messages in a SQLite 3 database. The project also provides the ability to add logs into a file using NLog. Below, you will find instructions on setting up and running the project locally.

## Prerequisites
Before you begin, ensure you have met the following requirements:

* Visual Studio 2022 or later.
* .NET SDK 7.0.
* SQLite 3 installed on your machine.
* [DB Browser for SQLite](https://sqlitebrowser.org/) for managing the SQLite database.

## Database Setup
The SQLite database for this project is created using the following SQL command:


```sql
CREATE TABLE "Messages" (
    "Id"	INTEGER NOT NULL UNIQUE,
    "TransactionRefNo"	TEXT NOT NULL,
    "RelatedRefNo"	TEXT,
    "Narrative"	TEXT NOT NULL,
    PRIMARY KEY("Id")
)
```
You can use DB Browser for SQLite or any other SQLite database management tool to create and interact with the database.

## Project Structure
The project follows the Repository pattern and is organized into logical layers, including Controllers, Services, and Repositories.

**Controllers**: Handle incoming HTTP requests and provide responses.
**Services**: Contain business logic for processing Swift MT799 messages.
**Repositories**: Responsible for database operations and data access.

## Swagger Documentation
This project includes Swagger documentation, which provides an interactive API documentation interface. You can access the Swagger UI by navigating to `/swagger` when the application is running locally.

## Setup and Run Locally
To set up and run the project locally, follow these steps:

1. Clone the repository to your local machine:

    ```bash
    git clone https://github.com/your-repo-url.git
    ```

1. Open the project in Visual Studio 2022.

1. Build the project to restore NuGet packages and compile the code.

1. Update the database connection string in the `appsettings.json` file to point to your SQLite database file:

    ```json
    "ConnectionStrings": {
        "DefaultConnection": "Data Source=your-database-file-path.db;"
    }
    ```

1. To configure Logging with NLog update the `nlog.config` file to specify the log file paths according to your working directory. Here's an example:

    ```xml
        <!-- Internal Log File -->       
        <nlog 
            internalLogFile="your-application-log-file.log">

            <!-- Your Application's Log File -->
            <target xsi:type="File" name="file" 
                fileName="your-application-log-file.log" 
                layout="${longdate} ${uppercase:${level}} ${message}" />
            </targets>
    </nlog>
    ```
    

1. Build the project again to apply the configuration changes.

1. Run the project by pressing `F5` or clicking the "Start" button in Visual Studio.

1. Access the Swagger documentation at `https://localhost:port/swagger` for API documentation and testing.

Now you have the Swift MT799 Web API up and running locally, ready to accept and process Swift MT799 messages.