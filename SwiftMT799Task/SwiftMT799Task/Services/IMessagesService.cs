﻿using SwiftMT799Task.Helpers;

namespace SwiftMT799Task.Services
{
    public interface IMessagesService
    {
        Task<Result> CreateAsync(IFormFile file);
    }
}
