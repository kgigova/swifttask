﻿using NLog;
using SwiftMT799Task.Helpers;
using SwiftMT799Task.Logger;
using SwiftMT799Task.Models;
using SwiftMT799Task.Repositories;

namespace SwiftMT799Task.Services
{
    public class MessagesService : IMessagesService
    {
        private readonly IMessagesRepository _repository;
        private ILoggerService _logger;

        public MessagesService(IMessagesRepository repository, ILoggerService logger)
        {
            this._repository = repository;
            this._logger = logger;
        }
        public async Task<Result> CreateAsync(IFormFile file)
        {
            try
            {
                _logger.LogDebug("MessageService: ParseFileAsync has been called.");
                Message message = await MT799Helper.ParseFileAsync(file);
                await this._repository.Create(message);
                return new Result { Success = true };
            }
            catch (Exception ex)
            {
                _logger.LogError($"MessageService: {ex.Message}");
                return new Result { Success = false, Error = new Error { Description = ex.Message } };
            }
        }
    }
}
