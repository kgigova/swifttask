﻿namespace SwiftMT799Task.Models
{
    public class Message
    {
        public int Id { get; set; }
        public string TransactionRefNo { get; set; }

        public string? RelatedRefNo { get; set; }

        public string Narrative { get; set; }
    }
}
