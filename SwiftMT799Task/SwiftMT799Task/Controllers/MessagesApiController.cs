﻿using Microsoft.AspNetCore.Mvc;
using SwiftMT799Task.Logger;
using SwiftMT799Task.Services;

namespace SwiftMT799Task.Controllers
{
    [Route("api/messages")]
    [ApiController]
    public class MessagesApiController : ControllerBase
    {
        private readonly IMessagesService _messagesService;
        private ILoggerService _logger;

        public MessagesApiController(IMessagesService messageService, ILoggerService logger)
        {
            this._messagesService = messageService;
            this._logger = logger;
        }

        [HttpPost("")]
        public async Task<IActionResult> CreateMessageAsync(IFormFile file)
        {
            _logger.LogInfo("MessageController: Create request succeeded!");
            var result = await this._messagesService.CreateAsync(file);
            if (result.Success)
            {

                return this.StatusCode(StatusCodes.Status201Created);
            }

            _logger.LogError($"MessageController: {result.Error.Description}");
            return this.StatusCode(StatusCodes.Status400BadRequest, result);
        }
    }
}
