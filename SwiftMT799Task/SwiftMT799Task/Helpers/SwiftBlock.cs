﻿namespace SwiftMT799Task.Helpers
{
    public class SwiftBlock
    {
        public int BlockNumber { get; set; }
        public string Content { get; set; }
    }
}
