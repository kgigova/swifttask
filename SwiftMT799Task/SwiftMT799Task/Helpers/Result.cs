﻿namespace SwiftMT799Task.Helpers
{
    public class Result
    {
        public bool Success { get; set; }

        public Error? Error { get; set; }
    }
}
