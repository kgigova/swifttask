﻿using SwiftMT799Task.Models;
using System.Text.RegularExpressions;

namespace SwiftMT799Task.Helpers
{
    public class MT799Helper
    {
        private static string SwiftBlockMatchRegex = @"\{[1-5]:.*?\}";
        private const string InvalidMT799ErrorMessage = "Not a valid MT799 message!";
        private const string InvalidSwiftErrorMessage = "Invalid Swift Message!";

        public static async Task<Message> ParseFileAsync(IFormFile file)
        {
            try
            {
                var reader = new StreamReader(file.OpenReadStream());
                var messageText = await reader.ReadToEndAsync();

                var swiftBlocks = ExtractSwiftBlocks(messageText);

                var messageBlock = swiftBlocks.Find(block => block.BlockNumber == 4);
                if (messageBlock == null)
                {
                    throw new Exception(InvalidSwiftErrorMessage);
                }

                string transactionReferenceNo = GetValueBetween(messageBlock.Content, ":20:", "\n");
                string relatedReferenceNo = GetValueBetween(messageBlock.Content, ":21:", "\n");
                string text = GetValueBetween(messageBlock.Content, ":79:", "\n-");


                var message = new Message
                {
                    TransactionRefNo = transactionReferenceNo,
                    RelatedRefNo = relatedReferenceNo,
                    Narrative = text
                };

                return message;
            }
            catch (Exception ex)
            {
                throw new Exception(InvalidMT799ErrorMessage);
            }
        }

        private static string GetValueBetween(string input, string start, string end)
        {
            int startIndex = input.IndexOf(start) + start.Length;
            int endIndex = input.IndexOf(end, startIndex);
            return input.Substring(startIndex, endIndex - startIndex).Trim();
        }

        private static SwiftBlock ParseSwiftBlock(string blockText)
        {
            // Extract the block number from the match
            int blockNumber = int.Parse(blockText.Substring(1, 1));

            // Extract the content within the block
            string content = blockText.Substring(4, blockText.Length - 5); // Removing opening and closing braces

            return new SwiftBlock
            {
                BlockNumber = blockNumber,
                Content = content
            };
        }

        private static List<SwiftBlock> ExtractSwiftBlocks(string messageText)
        {
            List<SwiftBlock> blocks = new List<SwiftBlock>();
            MatchCollection matches = Regex.Matches(messageText, SwiftBlockMatchRegex, RegexOptions.Singleline);

            foreach (Match match in matches)
            {
                blocks.Add(ParseSwiftBlock(match.Value));
            }

            return blocks;
        }
    }
}
