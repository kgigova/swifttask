﻿using SwiftMT799Task.Models;

namespace SwiftMT799Task.Repositories
{
    public interface IMessagesRepository
    {
        Task Create(Message message);
    }
}
