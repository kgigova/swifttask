﻿using SwiftMT799Task.Models;

namespace SwiftMT799Task.Repositories
{
    public class MessagesRepository : IMessagesRepository
    {
        private readonly DbContext _dbContext;

        public MessagesRepository(DbContext dbContext)
        {
            this._dbContext = dbContext;
        }
        public async Task Create(Message message)
        {
            var command = this._dbContext.GetConnection().CreateCommand();
            command.CommandText =
            @"
                INSERT INTO Messages (TransactionRefNo, RelatedRefNo, Narrative) VALUES (@TransactionRefNo, @RelatedRefNo, @Narrative)
            ";
            command.Parameters.AddWithValue("@TransactionRefNo", message.TransactionRefNo);
            command.Parameters.AddWithValue("@RelatedRefNo", message.RelatedRefNo);
            command.Parameters.AddWithValue("@Narrative", message.Narrative);
            await command.ExecuteNonQueryAsync();
        }
    }
}
