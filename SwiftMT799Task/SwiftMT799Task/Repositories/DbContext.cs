﻿using NLog;
using SwiftMT799Task.Helpers;
using SwiftMT799Task.Logger;
using System.Data.SQLite;

namespace SwiftMT799Task.Repositories
{
    public class DbContext : IDisposable
    {
        private SQLiteConnection _connection;
        private ILoggerService _logger;

        public DbContext(IConfiguration configuration, ILoggerService logger)
        {
            _connection = new SQLiteConnection(configuration.GetConnectionString("DefaultConnection"));
            _connection.Open();
            _logger = logger;
        }

        public SQLiteConnection GetConnection() 
        { 
            return _connection; 
        }

        public void Dispose()
        {
            try
            {
                _connection.Close();
                _connection.Dispose();
            }
            catch (Exception ex)
            {
                _logger.LogError($"DbContext: {ex.Message}");
            }
        }
    }
}
